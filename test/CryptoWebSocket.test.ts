import { assert } from 'chai';
import CryptoWebSocket from '../src/CryptoWebSocket';
import * as CONST from '../src/constants/wsSettingsConsts';
import * as ERROR from '../src/constants/errorConsts';
import sinon from 'sinon';

class WebSocket{
}

describe('tests CryptoWebSocket class', function() {
    sinon.stub(CryptoWebSocket.prototype, 'initWebSocket').returns({});
    it('should create CryptoWebSocket object', function() {
        let cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS);
        assert.notEqual(cryptoWebSocket, null);
    });

    it('when cryptoWebSocket is created it should have default values in its fields', function() {
        let cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS);
        cryptoWebSocket.webSocket = sinon.createStubInstance(WebSocket);
        assert.notEqual(cryptoWebSocket.webSocket, null);
        assert.notEqual(cryptoWebSocket.wsSettings, null);
        assert.equal(cryptoWebSocket.callbackContext, null);
        assert.equal(cryptoWebSocket.heartbeat_interval, null);
    });

    it('when cryptoWebSocket is created with context and error callback it should have expected values in its fields', function() {
        let callbackContext = { onCloseErrorCallback: () => {} };
        let cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS, null, callbackContext, 'onCloseErrorCallback');
        cryptoWebSocket.webSocket = sinon.createStubInstance(WebSocket);
        assert.notEqual(cryptoWebSocket.webSocket, null);
        assert.notEqual(cryptoWebSocket.wsSettings, null);
        assert.equal(cryptoWebSocket.callbackContext, callbackContext);
        assert.equal(cryptoWebSocket.errorCallback, 'onCloseErrorCallback');
        assert.equal(cryptoWebSocket.heartbeat_interval, null);
    });
    
    it('when webSocketOpen called heartbeat_interval field should not be null', function() {
        const cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS);
        cryptoWebSocket.webSocketOpen();
        assert.notEqual(cryptoWebSocket.heartbeat_interval, null);
        clearInterval(cryptoWebSocket.heartbeat_interval);
    });

    it('when valid data passed to webSocketOnMessage result should not be null and missed_heartbeats_limit should be minimum', function() {
        let resultData = JSON.stringify({ result: 'Something'});
        const cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS);
        const event = { data: resultData};
        cryptoWebSocket.webSocketOnMessage(event);
        assert.notEqual(cryptoWebSocket.result, null);
        assert.equal(cryptoWebSocket.wsSettings.missed_heartbeats_limit, cryptoWebSocket.wsSettings.missed_heartbeats_limit_min);
    });

    it('when valid data received from websocket, then parser function should return expected JSON object', function() {
        const cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS);
        const data = JSON.stringify({ 
            result: 'Result',
            secondResult: 'Second result',
            code: 'Error code',
            message: 'Message'
        });

        let parsedResult = cryptoWebSocket.parseWebSocketResponse(data);
        let parsedJson = JSON.parse(data);
        assert.notEqual(parsedResult, null);
        assert.equal(parsedResult.result, parsedJson.result);
        assert.equal(parsedResult.secondResult, parsedJson.secondResult);
        assert.equal(parsedResult.code, parsedJson.code);
        assert.equal(parsedResult.message, parsedJson.message);
    });

    it('when valid data received from websocket, then callback function is called', function() {
        const cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS, null, {},);
        const data = JSON.stringify({ 
            result: 'Result',
            secondResult: 'Second result',
            code: 'Error code',
            message: 'Message'
        });

        cryptoWebSocket.callbackContext = { getKeys: function() {
            console.log('getKeys');
        }};
        cryptoWebSocket.callback = 'getKeys';

        let parsedResult = cryptoWebSocket.parseWebSocketResponse(data);
        let executedCallback = cryptoWebSocket.executeCallback(parsedResult);
        assert.equal(executedCallback, true);
    });

    it('when error occured while connecting to WS server, then callback function is called with an error', function() {
        let callbackContext = { onCloseErrorCallback: () => {} };
        const cryptoWebSocket = new CryptoWebSocket(CONST.WS_ADDRESS, null, callbackContext, 'onCloseErrorCallback');
        let spyExecuteCallback = sinon.spy(cryptoWebSocket, 'executeCallback')

        cryptoWebSocket.webSocketOnClose({wasClean: false});
        assert(spyExecuteCallback.calledOnce)
        assert(spyExecuteCallback.withArgs({result: 'Connection error', secondResult: 'Connection error', code: ERROR.CONNECT_ERROR}).calledOnce)
    });
}); 