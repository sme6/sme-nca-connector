import { assert } from 'chai';
import CryptoObject from '../src/CryptoObject';
import { CryptoWebSocketResponse } from '../src/CryptoWebSocket';
import * as CONST from '../src/constants/wsSettingsConsts';
import * as ERROR from '../src/constants/errorConsts';
import * as MESSAGE from '../src/constants/responseMessage';
import { RESPONSE_ERROR, RESPONSE_OK } from '../src/constants/responseCodes';

describe('tests CryptoObject class', function() { 
    it('should create CryptoObject object with wsAddress', function() {
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        assert.notEqual(cryptoObject, null);
    });

    it('should create CryptoObject object with wsAddress and wsSettings', function() {
        
        const wsSettings = {
            heartbeat_msg: 'message',
            missed_heartbeats: 1,
            missed_heartbeats_limit_min: 1,
            missed_heartbeats_limit_max: 10
        };

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS,wsSettings);
        assert.notEqual(cryptoObject, null);
    });

    it('when generateBrowseKeyStoreData method called with valid data generated data should be as expected', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const storageName = 'storageName';
        const fileExtension = '.exe';
        const currentDirectory = '/currentDirectory';
        
        let browserKeyStore = cryptoObject.generateBrowseKeyStoreData(storageName, fileExtension, currentDirectory);
        let objectBrowserKeyStore = JSON.parse(browserKeyStore);
        assert.notEqual(browserKeyStore, null);
        assert.include(objectBrowserKeyStore.args, storageName, 'args contains storageName');
        assert.include(objectBrowserKeyStore.args, fileExtension, 'args contains fileExtension');
        assert.include(objectBrowserKeyStore.args, currentDirectory, 'args contains currentDirectory');
    });

    it('when generateGetKeysData  method called with valid data generated data should be as expected', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const storageName = 'storageName';
        const storagePath = '/storagePath';
        const password = '1234546';
        const type = 'type';
        
        let getKeys = cryptoObject.generateGetKeysData(storageName, storagePath, password, type);
        let objectGetKeys = JSON.parse(getKeys);
        assert.notEqual(getKeys, null);
        assert.include(objectGetKeys.args, storageName, 'args contains storageName');
        assert.include(objectGetKeys.args, storagePath, 'args contains storagePath');
        assert.include(objectGetKeys.args, password, 'args contains password');
        assert.include(objectGetKeys.args, type, 'args contains type');
    });

    it('when generateCreateCMSSignatureData method called with valid data generated data should be as expected', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const storageName = 'storageName';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = 'password';
        const dataToSign = '18.04.2018';
        const attached = 'attached';
        
        let cmsSignature = cryptoObject.generateCreateCMSSignatureData(storageName, storagePath, alias, password, dataToSign, attached);
        let objectCmsSignature = JSON.parse(cmsSignature);
        assert.notEqual(cmsSignature, null);
        assert.include(objectCmsSignature.args, storageName, 'args contains storageName');
        assert.include(objectCmsSignature.args, storagePath, 'args contains storagePath');
        assert.include(objectCmsSignature.args, alias, 'args contains alias');
        assert.include(objectCmsSignature.args, password, 'args contains password');
        assert.include(objectCmsSignature.args, dataToSign, 'args contains dataToSign');
        assert.include(objectCmsSignature.args, attached, 'args contains alias');
    });
    
    it('when generateCreateCMSSignatureFromBase64Data method called with valid data generated data should be as expected', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const storageName = 'storageName';
        const keyType = 'SIGNATURE';
        const base64ToSign = 'base64ToSign';
        const flag = true;     
        
        let cmsSignature = cryptoObject.generateCreatecMSignatureBase64Data(storageName, keyType, base64ToSign, flag);
        let objectCmsSignature = JSON.parse(cmsSignature);
        assert.notEqual(cmsSignature, null);
        assert.include(objectCmsSignature.args, storageName, 'args contains storageName');
        assert.include(objectCmsSignature.args, keyType, 'args contains keyType');
        assert.include(objectCmsSignature.args, base64ToSign, 'args contains base64ToSign');
        assert.include(objectCmsSignature.args, flag, 'args contains flag');
    });

    it('when generateGetActiveTokens method called with valid data generated data should be as expected', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const getActiveTokens = {
            module: "kz.gov.pki.knca.commonUtils",
            method: "getActiveTokens"
        };
        
        let generateGetActiveTokens = cryptoObject.generateActiveTokens();
        let objectCmsSignature = JSON.parse(generateGetActiveTokens);
        assert.notEqual(generateGetActiveTokens, null);
        assert.equal(objectCmsSignature.module, getActiveTokens.module, 'args contains module');
        assert.equal(objectCmsSignature.method, getActiveTokens.method, 'args contains method');
    });

    it('when createCMSSignatureFromBase64Callback method called, the output should return 200 string', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const webSocketResponse: CryptoWebSocketResponse = {
            code: RESPONSE_OK,
            message: 'Some message',
            result: 'result',
            secondResult: 'SecondResult',
        }    
        
        let cmsSignature = cryptoObject.createCMSSignatureFromBase64Callback(webSocketResponse);
        assert.equal(cmsSignature, RESPONSE_OK);
    });

    it('when createCMSSignatureFromBase64Callback method called, the output should return 500 string', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const webSocketResponse: CryptoWebSocketResponse = {
            code: RESPONSE_ERROR,
            message: 'Some message',
            result: 'result',
            secondResult: 'SecondResult',
        }    
        
        let cmsSignature = cryptoObject.createCMSSignatureFromBase64Callback(webSocketResponse);
        assert.equal(cmsSignature, RESPONSE_ERROR);
    });

    it('when getActiveTokensCallback method called, the output should return 200 string', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const webSocketResponse: CryptoWebSocketResponse = {
            code: RESPONSE_OK,
            message: 'Some message',
            result: 'result',
            secondResult: 'SecondResult',
        }    
        
        let cmsSignature = cryptoObject.getActiveTokensCallback(webSocketResponse);
        assert.equal(cmsSignature, RESPONSE_OK);
    });

    it('when getActiveTokensCallback method called, the output should return 500 string', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const webSocketResponse: CryptoWebSocketResponse = {
            code: RESPONSE_ERROR,
            message: 'Some message',
            result: 'result',
            secondResult: 'SecondResult',
        }    
        
        let cmsSignature = cryptoObject.getActiveTokensCallback(webSocketResponse);
        assert.equal(cmsSignature, RESPONSE_ERROR);
    });
    

    it('when parseWebsocketResponse is called with success input, the output should be success too', function(){
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const response = { 
            result: 'Result',
            secondResult: 'SecondResult',
            code: ERROR.NONE,
            message: ''
            };
        let parsedResponse = cryptoObject.parseWebsocketResponse(response);
        assert.notEqual(parsedResponse, null);
        assert.isObject(parsedResponse, 'parsedResponse is an object');
        assert.equal(parsedResponse.message, '');
        assert.equal(parsedResponse.code, ERROR.NONE);
        assert.equal(parsedResponse.result, response.result);
        assert.equal(parsedResponse.secondResult, response.secondResult);
    });

    it('when parseWebsocketResponse is called with WRONG_PASSWORD code and response.result is empty, the output should be error too', function() {
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const response = { 
            result: '',
            secondResult: '',
            code: ERROR.WRONG_PASSWORD,
            message: ''
            };
        let parsedResponse = cryptoObject.parseWebsocketResponse(response);
        assert.notEqual(parsedResponse, null);
        assert.isObject(parsedResponse, 'parsedResponse is an object');
        assert.equal(parsedResponse.result, '');
        assert.equal(parsedResponse.secondResult, '');
        assert.equal(parsedResponse.code, ERROR.WRONG_PASSWORD);
        assert.equal(parsedResponse.message, `Неправильный пароль! Количество оставшихся попыток: ${response.result}`);
    });

    it('when parseWebsocketResponse is called with WRONG_PASSWORD code but response.result not empty, the output should be error too', function() {
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const response = { 
            result: 'Result',
            secondResult: '',
            code: ERROR.WRONG_PASSWORD,
            message: ''
            };
        let parsedResponse = cryptoObject.parseWebsocketResponse(response);
        assert.notEqual(parsedResponse, null);
        assert.isObject(parsedResponse, 'parsedResponse is an object');
        assert.equal(parsedResponse.result, 'Result');
        assert.equal(parsedResponse.secondResult, '');
        assert.equal(parsedResponse.code, ERROR.WRONG_PASSWORD);
        assert.equal(parsedResponse.message, "Неправильный пароль!");
    });

    it('when parseWebsocketResponse is called without WRONG_PASSWORD and NONE code, the output should be default error', function() {
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        const response = { 
            result: 'Wrong something!',
            secondResult: '',
            code: '',
            message: ''
            };

        let parsedResponse = cryptoObject.parseWebsocketResponse(response);
        assert.notEqual(parsedResponse, null);
        assert.isObject(parsedResponse, 'parsedResponse is an object');
        assert.equal(parsedResponse.result, 'Wrong something!');
        assert.equal(parsedResponse.secondResult, '');
        assert.equal(parsedResponse.code, '');
        assert.equal(parsedResponse.message, `GET keys error: ${response.result}`);
    });
    it('when createCMSSignatureValidate is called should not return null', function(){
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = 'password';
        const dataToSign = '18.04.2018';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        assert.notEqual(cryptoObject.createCMSSignatureValidate(), null);
    });

    it('when createCMSSignatureValidate is called should not return null', function(){
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = 'password';
        const dataToSign = '18.04.2018';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);

        assert.isObject(cryptoObject.createCMSSignatureValidate(), 'should return object');
        assert.isObject(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, password, dataToSign), "should return object");
    });
    
    it('when createCMSSignatureValidate is called should return response message ', function(){
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = 'password';
        const dataToSign = '18.04.2018';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias).message, '');
        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, password, dataToSign).result, MESSAGE.SUCCESS);
    });

    it('when pass storageAlias, storagePAth, alias arguments createCMSSignatureValidate after calling should be return MESSAGE.ENTER_PASSWORD_TO_STORAGE. message', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);

        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, "").message, MESSAGE.ENTER_PASSWORD_TO_STORAGE);
    });

    it('when pass storageAlias, storagePAth, alias,password, empty dataToSign arguments createCMSSignatureValidate after calling should be return MESSAGE.DID_NOT_ENTER_DATA message', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = '121212122asa';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);

        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, password, "").message, MESSAGE.DID_NOT_ENTER_DATA);
    });

    it('when pass all arguments createCMSSignatureValidate after calling should be return MESSAGE.SUCCESS result and secondResult', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = '121212122asa';
        const dataToSign = "18.04.2018";

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);

        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, password, dataToSign).result, MESSAGE.SUCCESS);
        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, password, dataToSign).secondResult, MESSAGE.SUCCESS);
    });

    it('when pass storageAlias, storagePAth arguments createCMSSignatureValidate after calling should be return MESSAGE.ENTER_PASSWORD_TO_STORAGE. message', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);

        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, "", "").message, MESSAGE.ENTER_PASSWORD_TO_STORAGE);
    });

    it('when pass storageAlias, storagePAth, empty alias, password arguments createCMSSignatureValidate after calling should be return MESSAGE.DID_NOT_CHOOSE_KEY. message', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const password = "454527788asas";

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);

        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, "", password).message, MESSAGE.DID_NOT_CHOOSE_KEY);
    });

    it('when pass empty arguments createCMSSignatureValidate after calling should be return EMPTY_PATH code', function() {
        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);

        assert.equal(cryptoObject.createCMSSignatureValidate("", "", "").code, ERROR.EMPTY_PATH);
    });

    it('when pass storageAlias,storagePath, alias arguments createCMSSignatureValidate after calling should be return EMPTY_PASSWORD code', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, "").code, ERROR.EMPTY_PASSWORD);
    });

    it('when pass storageAlias,storagePath arguments createCMSSignatureValidate after calling should be return EMPTY_ALIAS code', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, "").code, ERROR.EMPTY_ALIAS);
    });
    
    it('when pass storageAlias,storagePath, alias, password arguments createCMSSignatureValidate after calling should return EMPTY_DATA code', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = '1212121212';

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, password, "").code, ERROR.EMPTY_DATA);
    });

    it('when pass storageAlias,storagePath, alias, password,dataToSign arguments createCMSSignatureValidate after calling should return SUCCESS result and secondResult', function() {
        const storageAlias = 'storageAlias';
        const storagePath = '/storagePath';
        const alias = 'alias';
        const password = '1212121212';
        const dataToSign = "12.08.17";

        const cryptoObject = new CryptoObject(CONST.WS_ADDRESS);
        assert.equal(cryptoObject.createCMSSignatureValidate(storageAlias, storagePath, alias, password, dataToSign).result, MESSAGE.SUCCESS);
    });
});