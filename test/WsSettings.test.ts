import { assert } from 'chai';
import WsSettings from '../src/WsSettings';
import * as CONST from "../src/constants/wsSettingsConsts";

describe('tests WsSettings class', function() {
    it('should create WsSettings object', function() {
        let wsSettings = new WsSettings();
        assert.notEqual(wsSettings, null);
    });

     it('when WsSettins is created it should have default values in its fields', function() {
        let wsSettings = new WsSettings();
        assert.equal(wsSettings.heartbeat_msg, CONST.HEARTBEAT_MSG);
        assert.equal(wsSettings.missed_heartbeats, CONST.MISSED_HEARTBEATS);
        assert.equal(wsSettings.missed_heartbeats_limit_min, CONST.MISSED_HEARTBEATS_LIMIT_MIN);
        assert.equal(wsSettings.missed_heartbeats_limit_max, CONST.MISSED_HEARTBEATS_LIMIT_MAX);
        assert.equal(wsSettings.missed_heartbeats_limit, CONST.MISSED_HEARTBEATS_LIMIT);
    });
    
    -it('when custom values are passed to the constructor wsSettings should have overwritten values in it fields', function() {
        let wsSettings = new WsSettings({
            heartbeat_msg: 'my message', 
            missed_heartbeats: 5, 
            missed_heartbeats_limit_min: 2,
            missed_heartbeats_limit_max: 51, 
            missed_heartbeats_limit: 30
        });
            
        assert.equal(wsSettings.heartbeat_msg, 'my message');
        assert.equal(wsSettings.missed_heartbeats, 5);
        assert.equal(wsSettings.missed_heartbeats_limit_min, 2);
        assert.equal(wsSettings.missed_heartbeats_limit_max, 51);
        assert.equal(wsSettings.missed_heartbeats_limit, 30);
    });
}); 