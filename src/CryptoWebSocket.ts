import WSSettings from './WsSettings';
import * as ERROR from './constants/errorConsts';

export interface CryptoWebSocketResponse {
    result: any,
    secondResult: string,
    code: string
    message: string;
}


class CryptoWebSocket {
    public webSocket: any;
    public callbackContext: any;
    public wsSettings: WSSettings;
    public heartbeat_interval: any = null;
    public result: any = null;
    public callback: string = '';
    public errorCallback: string = '';
    public callbackCalled: boolean = false;
    public wsAddress: string;
    public isConnectionOpened: boolean;
    public failedSendCount: number;

    constructor(wsAddress: string, wsSettings?: any, callbackContext?: object, errorCallback?: string) {
        this.isConnectionOpened = false;
        this.failedSendCount = 0;
        this.callbackContext = callbackContext;
        this.errorCallback = errorCallback;
        this.wsSettings = new WSSettings(wsSettings);
        this.wsAddress = wsAddress;
        console.log('Address:  ' + this.wsAddress);
        this.initWebSocket(wsAddress);
    }

    public initWebSocket(wsAddress: string) {
        this.webSocket = new WebSocket(wsAddress);
        this.webSocket.onopen = (event: any) => {
            this.webSocketOpen();
        }
        this.webSocket.onclose = (event: any) => {
            this.webSocketOnClose(event);
        }
        this.webSocket.onmessage = (event: any) => {
            this.webSocketOnMessage(event);
        }
    }

    public webSocketOpen() {
        if(this.heartbeat_interval === null) {
            this.heartbeat_interval = setInterval(() => { this.pingLayer() }, 2000);
        }
        console.info("Connection opened");
        this.isConnectionOpened = true;
    }
    
    public webSocketOnClose(event: any) {
        if (event.wasClean) {
            console.log('connection has been closed');
        } else {
            this.executeCallback({ result: 'Connection error', secondResult: 'Connection error', code: ERROR.CONNECT_ERROR});
        }
        console.log('Code: ' + event.code + ' Reason: ' + event.reason);
        this.isConnectionOpened = false;
    }

    public send(file: any, callback: string) {
        this.callback = callback;
        if(this.isConnectionOpened) {
            this.webSocket.send(file);
            this.failedSendCount = 0;
        } else {
            if(this.failedSendCount < 5) {
                setTimeout(() => {
                    this.send(file, callback);
                    this.failedSendCount++;
                }, 1000);
            }
        }
        
    }

    public webSocketOnMessage(event?: any) {
        if (event.data === this.wsSettings.heartbeat_msg) {
            this.wsSettings.missed_heartbeats = 0;
            return;
        }
    
        this.result = this.parseWebSocketResponse(event.data);
        
        this.executeCallback(this.result);
        this.setMissedHeartbeatsLimitToMin();
    }

    public parseWebSocketResponse(data: any): CryptoWebSocketResponse {
        let rw:any = null;
        if (data !== null) {
            let utfData = JSON.parse(data.toString('utf8'));
            rw = {
                result: utfData['result'] || utfData['responseObject'],
                secondResult: utfData['secondResult'],
                code: utfData['code'] || utfData['code'],
                message: utfData['message']
            };
        }

        return rw;
    }

    public executeCallback(rw: object, errorCallback?: string): boolean {
        if(this.callbackContext && this.callback){
            this.callbackContext[errorCallback ? errorCallback : this.callback](rw);
            return true;
        }
        return false;
    }

    public setMissedHeartbeatsLimitToMin() {
        this.wsSettings.missed_heartbeats_limit = this.wsSettings.missed_heartbeats_limit_min;
    }

    public setMissedHeartbeatsLimitToMax() {
        this.wsSettings.missed_heartbeats_limit = this.wsSettings.missed_heartbeats_limit_max;
    }

    private pingLayer() {
        // console.log("pinging...");
        // try {
        //     this.wsSettings.missed_heartbeats++;
        //     if (this.wsSettings.missed_heartbeats >= this.wsSettings.missed_heartbeats_limit)
        //         throw new Error("Too many missed heartbeats.");
        //         this.webSocket.send(this.wsSettings.heartbeat_msg);
        // } catch (e) {
        //     clearInterval(this.heartbeat_interval);
        //     this.heartbeat_interval = null;
        //     console.warn("Closing connection. Reason: " + e.message);
        //     this.webSocket.close();
        // }
    }
}

export default CryptoWebSocket;