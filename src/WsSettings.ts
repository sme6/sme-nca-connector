import * as CONST from './constants/wsSettingsConsts';

class WebSocketSettings {
    public heartbeat_msg: string = CONST.HEARTBEAT_MSG;
    public missed_heartbeats: number = CONST.MISSED_HEARTBEATS;
    public missed_heartbeats_limit_min: number = CONST.MISSED_HEARTBEATS_LIMIT_MIN;
    public missed_heartbeats_limit_max: number = CONST.MISSED_HEARTBEATS_LIMIT_MAX;
    public missed_heartbeats_limit: number = CONST.MISSED_HEARTBEATS_LIMIT;

     constructor(...inits: Partial<WebSocketSettings>[]) {
        (<any>Object).assign(this, ...inits);
    }
}

export default WebSocketSettings;
