import CryptoWebSocket from './CryptoWebSocket';
import { CryptoWebSocketResponse } from './CryptoWebSocket';
import * as ERROR from '../src/constants/errorConsts';
import * as MESSAGE from '../src/constants/responseMessage';
import { RESPONSE_ERROR, RESPONSE_OK } from '../src/constants/responseCodes';

type ParsedResponse = {
    message?: string;
    code?: string; 
    result?: any;
    secondResult?: string;
}

class CryptoObject {

    private createCMSSignaturePromiseResolve: (arg: any) => void;
    private createCMSSignaturePromiseReject: (arg: any) => void;
    private createCMSSignatureFromBase64Resolve: (arg: any) => void;
    private createCMSSignatureFromBase64Reject: (arg: any) => void;
    private getKeysResolve: (arg: any) => void;
    private getKeysReject: (arg: any) => void;
    private getActiveTokensResolve:(arg: any) => void;
    private getActiveTokensReject:(arg: any) => void;
    private browseKeyStoreResponse: (arg: any) => void;
    private browseKeyStoreReject: (arg: any) => void;
    private onCloseErrorCallbackReject: (arg: any) => void;
    private webSocket:any = null;

    constructor(wsAddress: string, wsSettings?: object) {
        this.webSocket = new CryptoWebSocket(wsAddress, wsSettings, this, 'onCloseErrorCallback');
        this.getKeysResolve = () => {};
        this.getKeysReject = () => {};
        this.createCMSSignaturePromiseResolve = () => {};
        this.createCMSSignaturePromiseReject = () => {};
        this.browseKeyStoreResponse = () => {};
        this.browseKeyStoreReject = () => {};
        this.onCloseErrorCallbackReject = () => {};
        this.createCMSSignatureFromBase64Resolve = () => {};
        this.createCMSSignatureFromBase64Reject = () => {};
        this.getActiveTokensResolve = () => {};
        this.getActiveTokensReject = () => {};
    } 

    public browseKeyStore(storageName: string, fileExtension: string, currentDirectory: string, callBack: string) {
        const browseKeyStore = this.generateBrowseKeyStoreData(storageName, fileExtension, currentDirectory);
        this.webSocket.setMissedHeartbeatsLimitToMax();
        this.webSocket.send(browseKeyStore, 'browseKeyStoreCallback');
    }

    public getKeys(storageName: string, storagePath: string, password: string, type: string, callBack: string) {
        const getKeys = this.generateGetKeysData(storageName, storagePath, password, type);
        this.webSocket.setMissedHeartbeatsLimitToMax();
        this.webSocket.send(getKeys, 'getKeyToSignCallback');

        return new Promise((resolve: any, reject: any) => {
            this.getKeysResolve = resolve;
            this.getKeysReject = reject;
        });
    }

    public createCMSSignature(storageAlias: string, storagePath: string, alias: string, password: string, dataToSign: string, attached: string) {
        const createCMSSignature = this.generateCreateCMSSignatureData(storageAlias, storagePath, alias, password, dataToSign, attached);
        this.webSocket.setMissedHeartbeatsLimitToMax();
        this.webSocket.send(createCMSSignature, 'createCMSSignatureCallback');
        
        let errorValidate = this.createCMSSignatureValidate(storageAlias, storagePath, alias, password, dataToSign);
        if (errorValidate.result !== "success") {
            return new Promise((resolve, reject) => {
                reject(errorValidate.message);
            });
        }

        return new Promise((resolve, reject) => {
            this.createCMSSignaturePromiseResolve = resolve;
            this.createCMSSignaturePromiseReject = reject;
        })
    }

    public getActiveTokens() {
        const getActiveTokens = this.generateActiveTokens();
        
        this.webSocket.setMissedHeartbeatsLimitToMax();
        this.webSocket.send(getActiveTokens, 'getActiveTokensCallback');

        return new Promise((resolve, reject) => {
            this.getActiveTokensResolve = resolve;
            this.getActiveTokensReject = reject;
        })
    }

    public createCMSSignatureFromBase64(storagName, keyType, base64ToSign, flag) {
        const createCMSSignatureFromBase64 = this.generateCreatecMSignatureBase64Data(storagName, keyType, base64ToSign, flag);
        
        this.webSocket.setMissedHeartbeatsLimitToMax();
        this.webSocket.send(createCMSSignatureFromBase64, 'createCMSSignatureFromBase64Callback');

        return new Promise((resolve, reject) => {
            this.createCMSSignatureFromBase64Resolve = resolve;
            this.createCMSSignatureFromBase64Reject = reject;
        });
    }

    public generateBrowseKeyStoreData(storageName: string, fileExtension: string, currentDirectory: string) {
        const browseKeyStore = {
            "method": "browseKeyStore",
            "args": [storageName, fileExtension, currentDirectory]
        };
        return JSON.stringify(browseKeyStore);
    }

    public generateGetKeysData(storageName: string, storagePath: string, password: string, type: string) {
        const getKeys = {
            "method": "geconstKeys",
            "args": [storageName, storagePath, password, type]
        };
        return JSON.stringify(getKeys);
    }

    public generateActiveTokens() {
        const getActiveTokens = {
            "module": "kz.gov.pki.knca.commonUtils",
            "method": "getActiveTokens"
        };

        return JSON.stringify(getActiveTokens);
    }

    public generateCreateCMSSignatureData(storageAlias: string, storagePath: string, alias: string, password: string, dataToSign: string, attached: string) {
        const createCMSSignature = {
            "method": "createCMSSignature",
            "args": [storageAlias, storagePath, alias, password, dataToSign, attached]
        };
        return JSON.stringify(createCMSSignature);
    }

    public generateCreatecMSignatureBase64Data(storageName, keyType, base64ToSign, flag) {
        const createCMSSignatureFromBase64 = {
            "module": "kz.gov.pki.knca.commonUtils",
            "method": "createCMSSignatureFromBase64",
            "args": [storageName, keyType, base64ToSign, flag]
        }
        
        return JSON.stringify(createCMSSignatureFromBase64);
    }

    public createCMSSignatureValidate(storageAlias?: string, storagePath?: string, alias?: string, password?: string, dataToSign?: string): ParsedResponse {
        if (storagePath !== "" && storageAlias !== "") {
            if (password !== "") {
                if (alias !== "") {
                    if (dataToSign !== "") {
                        return { message: "", result: MESSAGE.SUCCESS, secondResult: MESSAGE.SUCCESS  }
                    } else {
                        return { message: MESSAGE.DID_NOT_ENTER_DATA, result: "", secondResult: "", code: ERROR.EMPTY_DATA }
                    }
                } else {
                    return { message: MESSAGE.DID_NOT_CHOOSE_KEY, result: "", secondResult: "", code: ERROR.EMPTY_ALIAS }
                }
            } else {
                return { message: MESSAGE.ENTER_PASSWORD_TO_STORAGE, result: "", secondResult: "", code: ERROR.EMPTY_PASSWORD }
            }
        } else {
            return { message: MESSAGE.DID_NOT_CHOOSE_STORAGE, result: "", secondResult: "", code: ERROR.EMPTY_PATH }
        }
        return {}
    }
    public parseWebsocketResponse(response: CryptoWebSocketResponse): ParsedResponse {
        if(response.code === ERROR.NONE) {
            return { message: '', code: ERROR.NONE, result: response.result, secondResult: response.secondResult };
        } else {
            if(response.code === ERROR.WRONG_PASSWORD && response.result.length === 0) {
                return { message: `${MESSAGE.WRONG_PASSWORD_ATTEMPS} ${response.result}`, code: response.code, result: '', secondResult: '' };
            } else if(response.code === ERROR.WRONG_PASSWORD) {
                return { message: MESSAGE.WRONG_PASSWORD, code: response.code, result: response.result, secondResult: '' };
            } else if(response.code === ERROR.CONNECT_ERROR) {
                return { message: MESSAGE.CONNECTION_ERROR, code: response.code, result: response.result, secondResult: response.secondResult }   
            }else {
                return { message: `${MESSAGE.GET_KEYS} ${response.result}`, code: response.code, result: response.result, secondResult: '' };
            }
        }
        return {};
    }

    public getKeysToSignCallback(webSocketResponse: CryptoWebSocketResponse) {
        let response = this.parseWebsocketResponse(webSocketResponse);
        if(response.code === ERROR.NONE) {
            this.getKeysResolve(response);
        } else {
            this.getKeysReject(response);
        }
    }

    public createCMSSignatureCallback(webSocketResponse: CryptoWebSocketResponse) {
        let response = this.parseWebsocketResponse(webSocketResponse);
        if(response.code === ERROR.NONE) {
            this.createCMSSignaturePromiseResolve(response);
        } else {
            this.createCMSSignaturePromiseReject(response);
        }
    }

    public browseKeyStoreCallback(webSocketResponse: CryptoWebSocketResponse) {
        let response = this.parseWebsocketResponse(webSocketResponse);
        if(response.code === ERROR.NONE) {
            this.browseKeyStoreResponse(response);
        } else {
            this.browseKeyStoreReject(response);
        }
    }
    
    public onCloseErrorCallback(webSocketResponse: CryptoWebSocketResponse) {
        let response = this.parseWebsocketResponse(webSocketResponse);
        if(webSocketResponse.code === ERROR.CONNECT_ERROR) {
            this.onCloseErrorCallbackReject(response);
        }
    }

    public createCMSSignatureFromBase64Callback(webSocketResponse: CryptoWebSocketResponse) {
        if (webSocketResponse['code'] === RESPONSE_ERROR) {
            this.createCMSSignatureFromBase64Reject(webSocketResponse);
        } else if (webSocketResponse['code'] === RESPONSE_OK) {
            this.createCMSSignatureFromBase64Resolve(webSocketResponse);
        }
        
        return webSocketResponse['code']
    }

    public getActiveTokensCallback(webSocketResponse: CryptoWebSocketResponse) {
        if (webSocketResponse['code'] === RESPONSE_ERROR) {
            this.getActiveTokensReject(webSocketResponse);
        } else if (webSocketResponse['code'] === RESPONSE_OK) {
            this.getActiveTokensResolve(webSocketResponse);
        }

        return webSocketResponse['code']
    }
}


export default CryptoObject;