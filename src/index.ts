import CryptoObject from './CryptoObject';

const NCLayerConnector = (wsAddress: string, wsSettings?: object) => {
    return new CryptoObject(wsAddress, wsSettings);
};

export default NCLayerConnector;