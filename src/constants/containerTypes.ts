export const NONE: string = 'NONE';
export const PKCS12: string = 'PKCS12';
export const AKKaztokenStore: string = 'AKKaztokenStore';
export const AKKZIDCardStore: string = 'AKKZIDCardStore';
export const AKEToken72KStore: string = 'AKEToken72KStore';
export const AKJaCartaStore: string = 'AKJaCartaStore';
