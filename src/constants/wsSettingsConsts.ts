
export const HEARTBEAT_MSG: string = '--heartbeat--';
export const HEARTBEAT_INTERVAL: any = null;
export const MISSED_HEARTBEATS: number = 0;
export const MISSED_HEARTBEATS_LIMIT_MIN: number = 3;
export const MISSED_HEARTBEATS_LIMIT_MAX: number = 50;
export const MISSED_HEARTBEATS_LIMIT: number = MISSED_HEARTBEATS_LIMIT_MIN;
export const WS_ADDRESS = 'ws://127.0.0.1:13579/';